'use strict';

const Audit = require('lighthouse').Audit;

const MAX_API_TIME = 4000;

class LoadAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'api-audit',
            description: 'Api response',
            failureDescription: 'Api slow to response',
            helpText: 'Used to measure time from navigationStart to when the api response' +
            ' is shown.',

            requiredArtifacts: ['TimeToApi']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToApi;

        const belowThreshold = loadedTime <= MAX_API_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit;